package TestScript;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commonmethods.API_Trigger;
import commonmethods.TestNG_retry_Analyzer;
import commonmethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_TestScript extends API_Trigger {
	File logfolder;
	Response response;
	ResponseBody responseBody ;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createFolder("Patch_API");
	}

	@Test (retryAnalyzer = TestNG_retry_Analyzer.class , description = "validate the responseBody parameter of Patch_TC_1")

	public void validate_Patch_API() {
		 response = Patch_API_Trigger(patch_request_body(), patch_endpoint());
		 int statuscode = response.statusCode();

		 responseBody  = response.getBody();

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		// step 4 parse the RequestBody using json path extract RequestBody parameter

		JsonPath jsp_req = new JsonPath(patch_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// 4.1: Generate expected Date
		LocalDateTime currentDate = LocalDateTime.now();
		String expecteddate = currentDate.toString().substring(0, 11);

		// step 5 validate using TestNG Assertion
		Assert.assertEquals(statuscode, 200, "correct sttuscode is not found even after retrying for 5 time");

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to name sent in RequestBody");
		Assert.assertEquals(res_job, req_job, "Job in ResposneBody is not equal to job sent in RequestBody");
		Assert.assertEquals(res_updatedAt, expecteddate,
				"createdAt is in ResponseBody is not equal to createdAt in request");

	}
@AfterTest
	public void teardown() throws IOException {
		Utilities.createlogfile("Patch_API", logfolder, patch_endpoint(), patch_request_body(),

				response.getHeaders().toString(), responseBody.asString());
	}
}
