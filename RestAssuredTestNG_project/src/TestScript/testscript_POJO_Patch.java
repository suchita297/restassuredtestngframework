package TestScript;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import EnvironmentandRepository.Req_Repo_Pojo;
import POJO.POJO_Patch_API;
import commonmethods.API_Trigger;
import commonmethods.TestNG_retry_Analyzer;
import commonmethods.Utilities;
import io.qameta.allure.internal.shadowed.jackson.core.JsonProcessingException;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class testscript_POJO_Patch extends API_Trigger {
	File logfolder;
	Response response;
	Req_Repo_Pojo req_ob = new Req_Repo_Pojo();

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createFolder("Patch_API");
	}

	@Test(retryAnalyzer = TestNG_retry_Analyzer.class, description = "validate the responseBody parameter of Patch_TC_1")

	public void validate_Patch_API() throws JsonProcessingException {
		response = Patch_API_Trigger(req_ob.Patch_TC1(), patch_endpoint());
		int statuscode = response.statusCode();

		POJO_Patch_API responseBody = response.as(POJO_Patch_API.class);

		String res_name = responseBody.getName();
		String res_job = responseBody.getJob();
		String res_updatedAt = responseBody.getUpdatedAt();
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		// step 4 parse the RequestBody using json path extract RequestBody parameter

		JsonPath jsp_req = new JsonPath(req_ob.Patch_TC1());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// 4.1: Generate expected Date
		LocalDateTime currentDate = LocalDateTime.now();
		String expecteddate = currentDate.toString().substring(0, 11);

		// step 5 validate using TestNG Assertion
		Assert.assertEquals(statuscode, 200, "correct sttuscode is not found even after retrying for 5 time");

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to name sent in RequestBody");
		Assert.assertEquals(res_job, req_job, "Job in ResposneBody is not equal to job sent in RequestBody");
		Assert.assertEquals(res_updatedAt, expecteddate,
				"createdAt is in ResponseBody is not equal to createdAt in request");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createlogfile("Patch_API", logfolder, patch_endpoint(), patch_request_body(),

				response.getHeaders().toString(), response.getBody().asString());
	}
}
