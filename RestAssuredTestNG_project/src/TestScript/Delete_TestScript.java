package TestScript;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commonmethods.API_Trigger;
import commonmethods.TestNG_retry_Analyzer;
import commonmethods.Utilities;
import io.restassured.response.Response;

public class Delete_TestScript extends API_Trigger {
	File logfolder;
	Response response;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createFolder("Delete_API");
	}

	@Test (retryAnalyzer = TestNG_retry_Analyzer.class , description = "validate the responseBody parameter of Delete_TC_1")
	public void validate_Delete_API() {
		response = Delete_API_Trigger(delete_request_body(), delete_endpoint());
		int statuscode = response.statusCode();

		Assert.assertEquals(statuscode, 204, "correct sttuscode is not found even after retrying for 5 time");
	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createlogfile("Delete_API", logfolder, delete_endpoint(), delete_request_body(),
				response.getHeaders().toString(), response.getBody().asString());

	}

}
