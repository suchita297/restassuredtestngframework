package TestScript;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import EnvironmentandRepository.Req_Repo_Pojo;
import POJO.POJO_Post_API;
import commonmethods.API_Trigger;
import commonmethods.TestNG_retry_Analyzer;
import commonmethods.Utilities;
import io.qameta.allure.internal.shadowed.jackson.core.JsonProcessingException;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class testscript_POJO_Post extends API_Trigger {
	File logfolder;
	Response response;

	Req_Repo_Pojo req_ob = new Req_Repo_Pojo();

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createFolder("Post_API");
	}

	@Test(retryAnalyzer = TestNG_retry_Analyzer.class, description = "validate the responseBody parameter of post_TC_1")
	public void validate_post_API() throws JsonProcessingException {
		response = Post_API_Trigger(req_ob.Post_TC1(), post_endpoint());
		int statuscode = response.statusCode();
		POJO_Post_API responseBody = response.as(POJO_Post_API.class);
		String res_name = responseBody.getName();
		String res_job = responseBody.getJob();
		String res_id = responseBody.getId();
		String res_createdAt = responseBody.getCreatedAt();
		res_createdAt = res_createdAt.toString().substring(0, 11);

		// 3 : fetch the requestBody parameter

		JsonPath jsp_req = new JsonPath(req_ob.Post_TC1());

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// 5 : Generate expected Date

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// step 6: validation using TestNG Assertion
		Assert.assertEquals(statuscode, 201, "correct statuscode  and even after retrying for 5 time");
		Assert.assertEquals(res_name, req_name, " Name in ResponseBody is not equal to name sent in RequestBody");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to job sent in RequestBody");
		Assert.assertNotNull(res_id, "id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createlogfile("Post_API", logfolder, post_endpoint(), post_request_body(),
				response.getHeaders().toString(), response.getBody().asString());

	}

}