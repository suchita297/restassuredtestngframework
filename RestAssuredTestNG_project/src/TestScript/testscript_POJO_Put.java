package TestScript;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import EnvironmentandRepository.Req_Repo_Pojo;
import POJO.POJO_Post_API;
import POJO.POJO_Put_API;
import commonmethods.API_Trigger;
import commonmethods.TestNG_retry_Analyzer;
import commonmethods.Utilities;
import io.qameta.allure.internal.shadowed.jackson.core.JsonProcessingException;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class testscript_POJO_Put extends API_Trigger {
	File logfolder;
	Response response;
	Req_Repo_Pojo req_ob = new Req_Repo_Pojo();

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createFolder("Put_API");
	}
    
	@Test (retryAnalyzer = TestNG_retry_Analyzer.class , description = "validate the responseBody parameter of put_TC_1")
	public void validate_PUT_API() throws JsonProcessingException {
		response = Put_API_Trigger(req_ob.Put_TC1(), put_endpoint());
		int statuscode = response.statusCode();
		POJO_Put_API responseBody = response.as(POJO_Put_API.class);
		String res_name = responseBody.getName();
		String res_job = responseBody.getJob();
		String res_updatedAt = responseBody.getUpdatedAt();
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		// step 3 : parse the requestBody using json path extract the rquestBody
		// parameter
		JsonPath jsp_req = new JsonPath(req_ob.Put_TC1());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// step4 : Generate expected Date
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// step 5 : validate using TestNG Assertion
		Assert.assertEquals(statuscode, 200, "correct sttuscode is not found even after retrying for 5 time");

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to name sent in RequestBody");
		Assert.assertEquals(res_job, req_job, "Job in ResposneBody is not equal to job sent in RequestBody");
		Assert.assertEquals(res_updatedAt, expecteddate,
				"createdAt is in ResponseBody is not equal to createdAt in request");

	}
@AfterTest
	public void teardown() throws IOException {
	Utilities.createlogfile("Put_API", logfolder, put_endpoint(), put_request_body(),
			response.getHeaders().toString(), response.getBody().asString());

	}

}
