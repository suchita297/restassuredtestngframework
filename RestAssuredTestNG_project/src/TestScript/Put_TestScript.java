package TestScript;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commonmethods.API_Trigger;
import commonmethods.TestNG_retry_Analyzer;
import commonmethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_TestScript extends API_Trigger {
	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createFolder("Put_API");
	}
    
	@Test (retryAnalyzer = TestNG_retry_Analyzer.class , description = "validate the responseBody parameter of put_TC_1")
	public void validate_PUT_API() {
		response = Put_API_Trigger(put_request_body(), put_endpoint());
		int statuscode = response.statusCode();
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		// step 3 : parse the requestBody using json path extract the rquestBody
		// parameter
		JsonPath jsp_req = new JsonPath(put_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// step4 : Generate expected Date
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// step 5 : validate using TestNG Assertion
		Assert.assertEquals(statuscode, 200, "correct sttuscode is not found even after retrying for 5 time");

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to name sent in RequestBody");
		Assert.assertEquals(res_job, req_job, "Job in ResposneBody is not equal to job sent in RequestBody");
		Assert.assertEquals(res_updatedAt, expecteddate,
				"createdAt is in ResponseBody is not equal to createdAt in request");

	}
@AfterTest
	public void teardown() throws IOException {
	Utilities.createlogfile("Put_API", logfolder, put_endpoint(), put_request_body(),
			response.getHeaders().toString(), responseBody.asString());

	}

}
