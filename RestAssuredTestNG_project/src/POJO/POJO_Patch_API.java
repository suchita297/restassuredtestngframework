package POJO;

public class POJO_Patch_API {
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	private String name;
	private String job;
	private String updatedAt;

}
