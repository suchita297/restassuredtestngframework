package POJO;

public class POJO_GetAPI_Data_Array {
	private String id;
	private String email;
	private String first_name;
	private String last_name;
	private String avatar;
	
	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public String getLast_name() {
		return last_name;
	}

	public String getAvatar() {
		return avatar;
	}

	

}
