package POJO;

import java.util.List;

public class POJO_GetAPI_Parent {
	public int getPage() {
		return page;
	}
	public int getPer_page() {
		return per_page;
	}
	public int getTotal() {
		return total;
	}
	public int getTotal_pages() {
		return total_pages;
	}
	public List<POJO_GetAPI_Data_Array> getData() {
		return data;
	}
	public POJO_GetAPI_Support getSupport() {
		return support;
	}
	private int page;
	private int per_page;
	private int total;
	private int total_pages;
	private List<POJO_GetAPI_Data_Array> data;
	private POJO_GetAPI_Support support;
	
	

}
