package commonmethods;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class TestNG_retry_Analyzer implements IRetryAnalyzer {
      private int start = 0;
	  private int end = 5;
	  
	  public boolean retry(ITestResult result) {
	  if (start<end) {
		  String testcasename = result.getName();
	  System.out.println(testcasename+"failed in current Iteration" + start + ", hence retrying for"+(start+1));
	  start++;
	  return true;
		  
	  }

	return false;
	}

}
