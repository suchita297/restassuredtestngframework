package EnvironmentandRepository;

import java.io.IOException;
import java.util.ArrayList;

import commonmethods.Utilities;

public class RequestRepository extends Environment {
	public static String post_param_requestBody( String testcaseName) throws IOException {
		ArrayList<String> data=  Utilities.ReadExcelData("Post_API", "TC1");
		String key1 = data.get(1);
		String req_name = data.get(2);
		String key2= data.get(3);
		String req_job = data.get(4);
		String requestBody = "{\r\n"
				+ "    \""+key1+"\": \""+req_name+"\",\r\n"
				+ "    \""+key2+"\": \""+req_job+"\"\r\n"
				+ "}";
		return requestBody;
		
	}
	


	public static String put_request_body() {
		String requestBody ="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}" ;
		return requestBody;
	}
	public static String post_request_body() {
		String requestBody = "{\r\n"
				+ "    \"name\": \"suchita\",\r\n"
				+ "    \"job\": \"tester\"\r\n"
				+ "}";
		return requestBody;
	}
	public static String patch_request_body() {
		String requestBody ="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}" ;
		return requestBody;
	}
	public static String get_request_body() {
		String requestBody ="";
		return requestBody;
	}
	public static String delete_request_body() {
		String requestBody ="";
		return requestBody;
	}
	
	}


